<!DOCTYPE html>
<html lang="pt-br">
<?php require('rdd/head.php') ?>
    <div class="fcol-12 down" style="height: 100vh;background:url('img/Inkscape-tecnologia.svg') center left;background-size:cover;overflow:hidden;"><img src="img/Inkscape-tecnologia-border.svg" width="100%" alt=""/></div>
    <div class="vcol-12 gcol-2 allHeight centerV2" style="background:url('img/softGreen-background.svg') center center / cover;">
        <span class="h1 centerText" style="font-family:'KING';padding:50px;">Amplie a visão de sua empresa com site criativo que é.</span>
        <div class="h3" style="display:grid;grid-template-rows: 250px auto auto auto;height:100%;min-height:500px;">
            <span class="flex center h3" style="overflow-x:hidden; "><div class="flex centerA responsive_exemple"><div>Res</div><div>pon</div><div>sivo</div></div></span>
            <span class="flex center">SEO<img class="search_exemple" src="img/search.svg" height="50px" alt="search"/></span>
            <span class="flex center">Moderno</span>
            <span class="flex center animations_exemple"><div class="bounce animated infinite">Anima</div><div class="slideOutUp animated infinite">do</div></span>
        </div>
        <span class="h2 centerText your-business" style="font-family:'Pink-Rocket';padding:50px;">E o mais importante, desenvolvido pensando no seu negócio!</span>
    </div>
    <div class="content fcol-12 h4 wrap centerH" style="padding:0;" title="Escolha um serviço de qualidade!" role="main">
        <p class="fcol-12 h1 center centerText" style="padding:2vw 5vw;font-family:'Top-Secret';">Entenda nossos produtos.</p>
        <div class="fcol-m-6 fcol-g-4 product">
            <div style="background: url('img/break-background.svg'),#B16D6B;background-blend-mode: lighten;" class="" data-wow-delay="1s">
                <p class="fcol-12 centerH centerText h2">Página Panfleto</p>
                <p class="centerText">Uma página web em formato de planfleto.</p>
                <br>
                <p>Inclui:</p>
                <p><i class="fas fa-check-circle"></i> Pode-se ser desenhado a mão livre.</p>
                <p><i class="fas fa-check-circle"></i> Possível mesclar vetor e imagem comum.</p>
                <p><i class="fas fa-check-circle"></i> Montagem da paleta de cores.</p>
                <p><i class="fas fa-check-circle"></i> SEO para google.</p>
                <p><i class="fas fa-check-circle"></i> Roda em formato de arquivo SVG.</p>
                <p><i class="fas fa-check-circle"></i> Elementos extremamente personalizaveis.</p>
            </div>
        </div>
        <div class="fcol-m-6 fcol-g-4 product">
            <div style="background: url('img/break-background.svg'),#6D6BB1;background-blend-mode: lighten;" class="" data-wow-delay="1s">
                <p class="fcol-12 centerH centerText h2">Página Web (WordPress)</p>
                <p class="centerH centerText">Construção da interface do site, acoplado ao sistema do WordPress.</p>
                <br>
                <p><i class="fas fa-check-circle"></i> Desenvolvimento de tema.</p>
                <p><i class="fas fa-check-circle"></i> Menu.</p>
                <p><i class="fas fa-check-circle"></i> Animações.</p>
                <p><i class="fas fa-check-circle"></i> Montagem da paleta de cores.</p>
                <p><i class="fas fa-check-circle"></i> SEO para google.</p>
                <p><i class="fas fa-check-circle"></i> Responsividade da página (capacidade de adaptar a diferentes tamanhos de tela).</p>
                <p><i class="fas fa-check-circle"></i> Elementos extremamente personalizaveis.</p>
                <p><i class="fas fa-check-circle"></i> Galeria, zoom, Mapa, etc…</p>
            </div>
        </div>
        <div class="fcol-m-6 fcol-g-4 product">
            <div class="" data-wow-delay="1s" style="background: url('img/break-background.svg'),#6BB16D;background-blend-mode: lighten;">
                <p class="fcol-12 centerH centerText h2" title="Página Web">Página Web</p>
                <p class="centerH centerText">Construção da interface do site.</p>
                <br>
                <p>Inclui:</p>
                <p><i class="fas fa-check-circle"></i> Menu.</p>
                <p><i class="fas fa-check-circle"></i> Animações.</p>
                <p><i class="fas fa-check-circle"></i> Montagem da paleta de cores.</p>
                <p><i class="fas fa-check-circle"></i> SEO para Google.</p>
                <p><i class="fas fa-check-circle"></i> Responsividade da página (capacidade de adaptar a diferentes tamanhos de tela).</p>
                <p><i class="fas fa-check-circle"></i> Elementos extremamente personalizaveis.</p>
                <p><i class="fas fa-check-circle"></i> Galeria, zoom, Mapa, etc…</p>
            </div>
        </div>
    </div>
    <footer class="fcol-12 flex aroundH" style="background-color:#232323;color:#ffffff;">
        <div class="flex column center">
            <a href="https://www.facebook.com/pg/SnakeTag-213624329498536" target="_blank" title="Facebook" style="padding:40px 20px;"><img src="img/facebook.svg" style="height: 30px;width:154px;" alt="Facebook"></a>
            <a href="https://t.me/Jumper_Luko" target="_blank" title="Telegram" style="padding:40px 20px;"><img src="img/telegram.svg" style="height: 50px;" alt="Telegram"></a>
        </div>
        <div class="flex column center">
        <a href="mailto:snaketag.contato@gmail.com" target="_blank" title="Email" style="padding:40px 20px;"><img src="img/email.svg" style="height: 30px;width:46px;" alt="Email"></a></a>
            <a href="https://api.whatsapp.com/send?phone=5547997058876&text=Ol%C3%A1,%20Estava%20visitando%20o%20site%20snakeag.store.%20Est%C3%A1%20dispon%C3%ADvel?" target="_blank" title="Whatsapp" style="padding:40px 20px;"><img src="img/whatsapp.svg" style="height: 50px;" alt="Whatsapp"></a></a>
        </div>
    </footer>
</body>
</html>
